package com.epam.mvc.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Phrase {

    TASK("Please, enter the word"),
    GREETING("Hello"),
    ADDRESSER("world!");

    private final String value;
}
