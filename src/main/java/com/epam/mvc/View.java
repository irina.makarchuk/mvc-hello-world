package com.epam.mvc;

import com.epam.mvc.data.Phrase;

public class View {

    public void printMessage(Phrase message, Phrase value) {
        System.out.printf("%s %s%n", message.getValue(), value.getValue());
    }
}
