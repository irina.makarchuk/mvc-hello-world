package com.epam.mvc;

import com.epam.mvc.data.Phrase;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Model {
    private Phrase value;
}
