package com.epam.mvc;

import com.epam.mvc.data.Phrase;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Scanner;

import static com.epam.mvc.data.Phrase.*;

public class Controller {
    private final Model model;
    private final View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void processUser() {
        Scanner scanner = new Scanner(System.in);

        enterPhraseWithValue(scanner, GREETING);
        model.setValue(GREETING);
        enterPhraseWithValue(scanner, ADDRESSER);
        model.setValue(ADDRESSER);
        view.printMessage(GREETING, ADDRESSER);
    }

    public void enterPhraseWithValue(Scanner scanner, Phrase value) {
        view.printMessage(TASK, value);
        String[] words;
        do {
            words = StringUtils.split(scanner.nextLine());
        } while (Arrays.stream(words).noneMatch(word -> word.equals(value.getValue())));
    }
}
